# k0s files

* awx.yml
* k0sctl.yaml
  * config for k0sctl cluster
  * ```k0sctl apply```
* kustomization.yaml
  * config for awx kustomization
  * ```kubectl apply -k .```
* metallb-k0s.yml
  * ips config for metallb
  * ```kubectl apply -f metallb-k0s.yml```

## configure load balancer

```kubectl apply -f metallb-k0s.yml```

## install awx

```kubectl apply -k .```

Run this command twice after 30s or so, sometimes the CRDs fail on first run.

```kubectl apply -k .```
