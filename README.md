# AWX Mob Session

## prerequisites

For the purposes of this setup, a linux environment with python3 available and git installed is assumed.  This automation should run on macos (with homebrew), and possibly on WSL for windows.  This automation was only tested on Rocky 9.3. The vm creation playbooks require a working proxmox installation. You can create the vm(s) manually in another hypervisor and skip the proxmox section.

For the linux user, ~/bin is used for tool.

Create ~/bin directory.

```
mkdir ~/bin
```

Ensure ~/bin is in your path. For bash edit ~/.bashrc and append the following lines to the bottom.
 
```
if [ -d "${HOME}/bin" ]; then
    export PATH="${PATH}:${HOME}/bin"
fi
```

Apply .bashrc changes to the current shell (or you can relogin).

```
source ~/.bashrc
```

### dev environment

Clone the awx-mod-sessions gitlab repo

```
git clone https://gitlab.com/sofreeus/awx-mob-sessions.git
```

CD into the newly cloned repo

```
cd awx-mob-sessions
```

Create a python3 venv

```
python3 -m venv venv --prompt awx
```

Activate python3 venv

```
source venv/bin/activate
```

Install ansible

```
pip3 install ansible
```

Install required python modules (proxmoxer, jmespath)

```
pip3 install -r collections/requirements.txt
```

Install required ansible collections

```
ansible-galaxy collection install -r collections/requirements.yml
```

### proxmox vms

Create proxmox vms.  For a multiple vm cluster use the group k0s instead of k0s_dev.

```
ansible-playbook playbooks/create-promox-vm.yml --limit k0s_dev
```

#### proxmox manual step

In proxmox the ansible playbook does not generate the cloud-init iso for the new vm.

Go to the proxmox properties page for the vm(s) and choose cloud-init on the left panel. click regenerate images. 

#### proxmox post-build tasks

Run post-build tasks. For a multiple vm cluster use the group k0s instead of k0s_dev.

```
ansible-playbook playbooks/proxmox-post-build.yml --limit k0s_dev
```

### k0sctl

Install k0sctl

Run this playbook only once to install k0sctl into ~/bin of your current user.

```
ansible-playbook playbooks/install-k0sctl.yml
```

### kubectl

Install kubectl

Run this playbook only once to install kubectl into ~/bin of your current user.

```
ansible-playbook playbooks/install-kubectl.yml
```

### k0s cluster

Deploy cluster from k0s-dev directory. For prod cd to k0s.

```
cd k0s-dev
k0sctl apply
```

### kubeconfig

Create kubeconfig file.  Use ~/.kube/k0s for multiple vm cluster.

```
cd k0s-dev
k0sctl kubeconfig > ~/.kube/k0s-dev
```

### awx

Deploy awx (run twice for CRDs)

```
cd k0s-dev
kubectl apply -k .
```

Wait 10-30 seconds for kubernetes CRDs and re-apply

```
cd k0s-dev
kubectl apply -k .
```

Get pod info.

```
kubectl get pods -A
```

### metallb

Wait for metallb namespace to show up in `kubectl get pods -A`

Deploy metallb config. Use metallb-k0s.yml for the multiple vm cluster.

```
cd k0s
kubectl apply -f metallb-k0s-dev.yml
```

### post build

Wait about 10 minutes for awx to be fully built.

Get the admin password

```
kubectl -n awx get secret awx-admin-password -o jsonpath="{.data.password}" | base64 --decode ; echo
```

Login to the web console (use awx.int.sofree.us for prod

[http://awx.int.sofree.us](http://awx.int.sofree.us)
[http://awx-dev.int.sofree.us](http://awx-dev.int.sofree.us)

* **User**: admin
* **Password**: from previous step

Generate an api token for the admin user

Click on username in the upper right corner

Choose "User Details"

Click the Tokens tab

Click the Add button

* **Description**: admin api token
* **Scope**: Write

Copy token from the pop-up window, and save to password manager/vault

Configure awx from awx.yml playbook

Requires the admin api token created in previous step

```
ansible-playbook awx.yml
```

### see also

* [sfs proxmox](https://proxmox.int.sofree.us)
* [kubernetes tools documentation](https://kubernetes.io/docs/tasks/tools/)
* [awx-operator releases](https://github.com/ansible/awx-operator/releases)
* [awx-ee images](https://quay.io/repository/ansible/awx-ee?tab=tags)
* [awx-operator kustomize spec](https://github.com/ansible/awx-operator/blob/devel/config/crd/bases/awx.ansible.com_awxs.yaml)
* [sfs rap](https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/sfs-rap-cue.md)
* [podman documentation](https://docs.podman.io/en/latest/)
