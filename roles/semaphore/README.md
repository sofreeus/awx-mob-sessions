# semaphore role

Create a semaphore container in podman.

Edit inventory/group_vars/semaphore to configure.

### Example

```
---
semaphore_container_version: "latest"
semaphore_image_base: "docker.io/semaphoreui"
semaphore_image: "{{semaphore_image_base}}/semaphore:{{semaphore_container_version}}"
semaphore_postgres_password: "somepassword123"
semaphore_admin_username: "admin"
semaphore_admin_name: "Admin"
semaphore_admin_email: "admin@example.com"
semaphore_admin_password: "changeme"
semaphore_ports: "3000:3000"
# generate with: head -c32 /dev/urandom | base64
semaphore_encryption_key: "qYEmCwWnms0xOZOHb3rCieiBHIAWItBGQM8eOFL0kFw="
semaphore_ldap_activated: "no"
semaphore_ldap_host: "dc01.example.com"
semaphore_ldap_port: "636"
semaphore_ldap_needtls: "yes"
semaphore_ldap_dn_bind: "uid=bind_user,cn=users,cn=accounts,dc=local,dc=example,dc=com"
semaphore_ldap_password: "somepassword123"
semaphore_ldap_dn_search: "dc=local,dc=example,dc=com"
# ldap search & = \u0026
semaphore_ldap_search_filter: "(\u0026(uid=%s)(memberOf=cn=ipausers,cn=groups,cn=accounts,dc=local,dc=example,dc=com"
```

### See Also

[ansible-semaphore github](https://github.com/ansible-semaphore/semaphore)
