# k0sctl

This role will install k0sctl v1.27.2+k0s.0 into the local environment.

See roles/k0sctl/defaults/main.yml

Refer to the [k0sctl github project](https://github.com/k0sproject/k0sctl) for more information.

k0sctl is used to deploy and upgrade a k0s kubernetes cluster.

Officially recommended for k0s in a production environment.

Documentation for [k0sctl](https://docs.k0sproject.io/v1.27.2+k0s.0/k0sctl-install/)

### Build a cluster

**Required** k0sctl must be installed locally

Use the config from sfs-sysops/sfs-k0s/sfs-k0s.yaml: 

```
cd sfs-k0s
k0sctl apply --config sfs-k0s.yaml
```

### Configure kubectl

**Optional** run if kubectl is installed locally

Use the config from sfs-sysops/sfs-k0s/sfs-k0s.yaml:

```
cd sfs-k0s
k0sctl kubeconfig --config sfs-k0s.yaml > ~/.kube/config
```

#### See Also:

    * roles/kubectl
