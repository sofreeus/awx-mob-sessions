# semaphore

## install podman

Install podman packages (podman-docker is optional)

```
yum install podman podman-plugins podman-docker
```

Install podman-compose

```
pip3 install podman-compose
```

## podman-compose.yml

Deploy podman-compose.yml from ansible playbook.

```
ansible-playbook playbooks/semaphore.yml
```

## bring up semaphore container

```
cd ~/semaphore
podman-compose up -d
```

## verify semaphore came up

Verify podman containers are running.

```
podman ps

CONTAINER ID  IMAGE                                    COMMAND               CREATED       STATUS       PORTS                   NAMES
25c501d1414f  docker.io/library/postgres:14            postgres              33 hours ago  Up 33 hours                          semaphore_postgres
03469c9edc31  docker.io/semaphoreui/semaphore:v2.9.45  /usr/local/bin/se...  33 hours ago  Up 33 hours  0.0.0
```

Verify semaphore url loads.

[semaphore localhost](http://localhost:3000)
[semaphore elvira](http://elvira.int.sofree.us:3000)
