# How to build the k0s cluster.

### Add k0s servers to ansible inventory

Edit the inventory file and add the following block:

```
[k0s_controllers]
k0s-dev.int.sofree.us ansible_host=192.168.73.47 # k0s controller

[k0s_workers]
k0s-dev.int.sofree.us ansible_host=192.168.73.47 # k0s worker

[k0_dev:children]
k0s_controller
k0s_worker

[k0s_dev:vars]
ansible_ssh_user=k0s
```

### Ensure inventory host_vars

Create the host_vars files each vm.

In each host_vars/hostname.int.sofree.us file:

Example (k0s-dev.int.sofree.us):

```
vm:
  name: k0s-dev.int.sofree.us
  memory: 6144
  sockets: 1
  cores: 6
  # host cpu required for rocky 9+
  cpu: host
  ostype: l26
  clone: rocky-9-cloud
  full: true
  storage: local-lvm
  node: proxmox
  description: "k0s-dev controller+worker"
  disks:
    # disk added only to workers, omit disks section for controllers
    # add 40G disk, rocky cloud image uses scsi0 with 10G disk
    - name: scsi1
      storage: local-lvm
      size: 40
      backup: false
  sshkeys: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEQxVZ7BMnOEc+nZQMMVjgh8QEjLG0JiwSoG0LcZRJAz mwhitford@esmerelda"
  ciuser: k0s
  searchdomains: "int.sofree.us"
  nameservers:
    - "1.1.1.1"
    - "8.8.8.8"
  ipconfig:
    ipconfig0: "gw=192.168.73.254,ip=192.168.73.47/24,ip6=auto"
```

### Use k0sctl to create the k0s cluster

```
cd k0s
k0sctl apply -f k0sctl-k0s-dev.yml
```

### Use k0sctl to create a kubectl config

```
cd k0s
k0sctl kubeconfig --config k0sctl-k0s-dev.yml > ~/.kube/k0s-dev
```

### Use kubectl to verify the cluster is up and running

```
kubectl cluster-info

Kubernetes control plane is running at https://192.168.73.47:6443
CoreDNS is running at https://192.168.73.47:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

kubectl get nodes -A

NAME                   STATUS   ROLES    AGE   VERSION
k0s-dev.int.sofree.us   Ready    <none>   79s   v1.27.2+k0s

kubectl get pods -A

NAMESPACE     NAME                              READY   STATUS    RESTARTS   AGE
kube-system   coredns-878bb57ff-rz2gj           1/1     Running   0          2m7s
kube-system   coredns-878bb57ff-zhwc8           1/1     Running   0          2m1s
kube-system   konnectivity-agent-4mhtt          1/1     Running   0          2m4s
kube-system   konnectivity-agent-bh46v          1/1     Running   0          2m5s
kube-system   konnectivity-agent-jjmb2          1/1     Running   0          2m4s
kube-system   kube-proxy-47b56                  1/1     Running   0          2m5s
kube-system   kube-proxy-hn9lg                  1/1     Running   0          2m4s
kube-system   kube-proxy-lmfmp                  1/1     Running   0          2m4s
kube-system   kube-router-5d84h                 1/1     Running   0          2m4s
kube-system   kube-router-bgnkw                 1/1     Running   0          2m4s
kube-system   kube-router-rcqbs                 1/1     Running   0          2m5s
kube-system   metrics-server-7f86dff975-lfgqn   1/1     Running   0          2m7s

```

### Troubleshooting Commands

```
kubectl cluster-info
kubectl cluster-info dump
kubectl get nodes -A
kubectl get pods -A
kubectl -n awx logs -f deployments/awx-operator-controller-manager -c awx-manager
kubectl -n awx get pod -o=custom-columns='NAME:spec.containers[*].name,IMAGE:spec.containers[*].image'
```
